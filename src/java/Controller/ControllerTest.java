/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import Hibernate.Contoh;
import ServiceDAO.ContohDao;
import ServiceDAO.ContohRepoIml;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.transaction.UserTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */

@Controller
public class ControllerTest 
{
    
    @Autowired
    ContohDao cek;
    
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String view(ModelMap model)
    {
        List<Contoh> data = cek.findALL();
        model.addAttribute("data", data);
        
        return "viewContoh";
    }
    
    @RequestMapping(value = "/input", method = RequestMethod.GET)
    public String input()
    {
        return "InputContoh";
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String post(@RequestParam("nama") String nama, @RequestParam("nim") String nim, ModelMap model)
    {
        
        Contoh contoh = new Contoh(nim, nama);
        cek.save(contoh);
        model.addAttribute("NAMA", nama);
        
        return "InputContoh";
    }
    
    @RequestMapping(value = "/delete", method = {RequestMethod.GET, RequestMethod.DELETE})
    public String delete(@RequestParam("nim") String nim, ModelMap model)
    {
        Contoh c = new Contoh();
        c.setNim(nim);
        
        cek.delete(c);
        
        List<Contoh> data = cek.findALL();
        model.addAttribute("data", data);
        model.addAttribute("berhasil", "Deleted !!!");
        return "viewContoh";
    }
   
}
