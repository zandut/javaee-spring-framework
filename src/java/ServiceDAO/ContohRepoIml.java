/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceDAO;

import Hibernate.Contoh;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
@Repository
public class ContohRepoIml implements ContohDao<Contoh>
{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Contoh t)
    {

        Session session = sessionFactory.openSession();
        
        Transaction tran = session.beginTransaction();

        session.save(t);
        tran.commit();
        session.close();

    }

    @Override
    public void delete(Contoh t)
    {
        Session session = sessionFactory.openSession();
        
        Transaction tran = session.beginTransaction();

        session.delete(t);
        tran.commit();
        session.close();
    }

    @Override
    public Contoh findByNIM(String ID)
    {
        Session session = sessionFactory.openSession();
        Transaction tran = session.beginTransaction();
        
        Contoh cont = (Contoh) session.get(Contoh.class, ID);
        session.close();
        return cont;
    }
    
    
    

    @Override
    public List<Contoh> findALL()
    {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Contoh> list =  session.createCriteria(Contoh.class).list();
        session.close();
        return list;
    }

}
