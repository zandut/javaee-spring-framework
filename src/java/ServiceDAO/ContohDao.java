/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServiceDAO;

import java.util.List;



/**
 *
 * @author Zandut <mfauzan613110035@gmail.com>
 */
public interface ContohDao<T>
{
    public void save(T t);
    public void delete(T t);
    public List<T> findALL();
    public T findByNIM(String string);
}
