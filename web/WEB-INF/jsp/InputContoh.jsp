<%-- 
    Document   : InputContoh
    Created on : Oct 26, 2016, 3:04:22 PM
    Author     : Zandut <mfauzan613110035@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Input Data</title>
    </head>
    <body>
        
        <form action="add" method="post">
            <input type="text" name="nim" /><br>
            <input type="text" name="nama" /><br>
            <input type="submit" name="Simpan" />
        </form>
        
        <% 
            if (request.getAttribute("NAMA") != null)
            {
        %>
                <h1>${NAMA} inserted !!</h1>
        <% 
            }
        %>
        
        <a href="hello">View Data</a>
    </body>
</html>
