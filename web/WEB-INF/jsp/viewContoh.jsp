<%@page import="Hibernate.Contoh"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <table>
            <thead>
                <tr>
                    <td>NIM</td>
                    <td>NAMA</td>
                    <td>AKSI</td>
                </tr>
            </thead>
            <tbody>
                <%
                    List<Contoh> data = (List<Contoh>) request.getAttribute("data");
                    for (Contoh c : data)
                    {


                %>

                <tr>
                    <td><%= c.getNim()%></td>
                    <td><%= c.getNama()%></td>
                    <td><a href="edit?nim=<%= c.getNim()%>" style="margin-right: 10px">Update</a><a href="delete?nim=<%= c.getNim()%>">Delete</a></td>
                </tr>

                <%

                    }
                %>
            </tbody>
        </table>
        <h2>${berhasil}</h2>
        <a href="input">Input Data</a>
    </body>
</html>
